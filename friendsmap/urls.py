from django.conf.urls import patterns, include, url
from django.views.generic.simple import redirect_to

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'friendsmap.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

	url(r'^$', 'neo4j.views.home', name="home"),
	(r'^login/$', redirect_to, {'url': '/facebook/example/'}),
	# (r'^facebook/connect/$', redirect_to, {'url': '/'}),

	(r'^facebook/', include('django_facebook.urls')),
	(r'^accounts/', include('django_facebook.auth_urls')), #Don't add this line if you use django registration or userena for registration and auth.

    url(r'^admin/', include(admin.site.urls)),
)
