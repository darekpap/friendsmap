from neo4django.db import models
from django_facebook.models import FacebookUser

class Person(models.NodeModel):
    name = models.StringProperty()

    friends = models.Relationship('self',rel_type='friends_with')

def create_user_profile(sender, instance, created, **kwargs):  
    if created:  
       profile, created = Person.objects.get_or_create(user=instance)  
