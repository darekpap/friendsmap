from django.shortcuts import render_to_response
from django_facebook.decorators import facebook_required
from django.template.context import RequestContext
from django_facebook.utils import next_redirect, parse_signed_request


@facebook_required(canvas=True)
def home(request, graph):
    '''
    Example of a canvas page.
    Canvas pages require redirects to work using javascript instead of http headers
    The facebook required and facebook required lazy decorator abstract this away
    '''
    context = RequestContext(request)
    signed_request_string = request.POST.get('signed_request')
    signed_request = {}
    if signed_request_string:
        signed_request = parse_signed_request(signed_request_string)
    context['signed_request'] = signed_request
    friends = []
    if graph:
        friends = graph.get('me/friends')['data']
    context['friends'] = friends

    return render_to_response('django_facebook/canvas.html', context)
